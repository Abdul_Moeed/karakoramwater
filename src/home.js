import React, {Component} from 'react';
import {View, StyleSheet, ScrollView, AsyncStorage, Dimensions} from 'react-native';
import {Actions} from 'react-native-router-flux';
import DrawerLayout from 'react-native-drawer-layout';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {Spinner, Button, Headbar, Card} from './component';
import SidemenuItems from './component/sidemenuItems'
import container from './component/stylesheet';

import {categoryAction} from './redux/actions/categoryaAction'

class home extends Component{

    componentWillMount(){
        this.props.categoryAction()
    }

    renderView(){
        
      return(
        this.props.category.map((x)=>{
            return  (
                <Card source={require('./assets/logo.png')}
                navigation={()=>{
                    AsyncStorage.setItem('type', x.Liter),
                    AsyncStorage.setItem('price', x.Price)
                    Actions.quantity()
                }}
                text={'Volume:  '+x.Liter}
                text1={'Price:  '+x.Price}/>
            )
        })
        
      )
    }


    render(){
        const sidemenu = <SidemenuItems/>
        return(
            <View style={container.container}>
            <DrawerLayout
                drawerWidth={220}
                ref={(drawer)=>{return this.drawer = drawer}}
                keyboardDismissMode='on-drag'
                renderNavigationView={()=> sidemenu}>
                <Headbar text={'Products'} source={require('./assets/menu.png')} navigation={() => this.drawer.openDrawer()}/>
                <ScrollView style={styles.scrollContainer}>
                <View style={styles.container}>
                {this.renderView()}
                </View>
                </ScrollView>
                </DrawerLayout>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    scrollContainer:{
        flex:1,
        width:Dimensions.get('window').width
    },
    container:{
        flex:1,
        flexDirection: 'row',
        flexWrap:'wrap',
        padding:2
    },
})

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        categoryAction
    },dispatch)
}

function mapStateToProps(state){
    return{
        category:state.category.category
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(home)