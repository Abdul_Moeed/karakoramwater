import {login} from './index';
import axios from 'axios';
import {loginUrl} from '../../component/baseUrl'

export function loginAction(login){
    return function (dispatch) {
        axios.post(`${loginUrl}`, login)
        .then(function (response){
            dispatch({type: 'login', payload: response.data})
        })
        .catch(function (err){
            dispatch({type:'login_reject', payload:err})
        })
    }
}