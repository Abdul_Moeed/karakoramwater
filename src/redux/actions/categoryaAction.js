import {category} from './index';
import axios from 'axios';
import {categoryUrl} from '../../component/baseUrl'

export function categoryAction(category){
    return function (dispatch) {
        axios.get(`${categoryUrl}`, category)
        .then(function (response){
            dispatch({type: 'category', payload: response.data})
        })
        .catch(function (err){
            dispatch({type:'category_reject', payload:err})
        })
    }
}