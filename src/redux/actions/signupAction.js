import {signup} from './index';
import axios from 'axios';
import {signupUrl} from '../../component/baseUrl'

export function signupAction(signup){
    return function (dispatch) {
        axios.post(`${signupUrl}`, signup)
        .then(function (response){
            dispatch({type: 'signup', payload: response.data})
        })
        .catch(function (err){
            dispatch({type:'signup_reject', payload:alert('Network Error')})
        })
    }
}