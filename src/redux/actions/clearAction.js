import {clear} from './index'

export function clearAction(){
    return{
        type:'clear',
        payload: 'State is clear'
    }
}