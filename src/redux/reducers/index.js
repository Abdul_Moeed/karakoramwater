import {combineReducers} from 'redux';
import {loginReducer} from './loginReducer';
import {signupReducer} from './signupReducer';
import {categoryReducer} from './categoryReducer'




const appReducer = combineReducers({
    login:loginReducer,
    signup:signupReducer,
    category:categoryReducer
})

const Reducers = (state, action) => {
    if (action.type === 'clear') {
        state = undefined
    }

    return appReducer(state, action)
}

export default Reducers