import React, {Component} from 'react';
import {View, StyleSheet, Image, AsyncStorage} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {Spinner} from './component';

class splash extends Component {

    constructor(){
        super();
        this.state={
            id:''
        }
    }
    closeSplash=()=>setTimeout(()=> this.navigationAction(),3000)

    componentDidMount(){
        this.closeSplash()
        AsyncStorage.getItem('userId').then((id)=>{
            this.setState({id:id})
        })
    }

    navigationAction(){
        if(this.state.id !== null){
           return Actions.home()
        }
        else{
           return Actions.login()
        }
    }


    componentDidMount(){
        this.closeSplash();
    }


    render(){
        return(
            <View style={styles.container}>
                <Image style={styles.img} source={require('./assets//logo.png')} />
                <Spinner/>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container:{
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#ced4ff',
        width:'100%',
        height:'100%'
    },
    img:{
        height:200,
        width:200
    }

})

export default splash;