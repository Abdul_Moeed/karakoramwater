import React from 'react';
import {TouchableOpacity, Text, Platform, Image} from 'react-native';

const Button = (props) => {
    return(
        <TouchableOpacity style={styles.buttonView} onPress={props.navigation} >
            <Text style={styles.buttonText}>{props.text}</Text>
            <Image style = {styles.img} source={props.source}/>
        </TouchableOpacity>
    )
}

const SmallButton = (props) => {
    return(
        <TouchableOpacity style={stylesSmall.buttonView} onPress={props.navigation} >
            <Text style={stylesSmall.buttonText}>{props.text}</Text>
            <Image style = {stylesSmall.img} source={props.source}/>
        </TouchableOpacity>
    )
}

const styles = {
    buttonView:{
        height:70,
        width:'80%',
        backgroundColor:'#bababa',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:5,
        borderWidth:3,
        borderColor:'#282828'
    },
    buttonText:{
        fontSize:30,
        fontWeight:'bold',
        color:'#282828'
    },
    img:{
        height:70,
        width:70
    }
};

const stylesSmall = {
    buttonView:{
        height:60,
        width:'40%',
        backgroundColor:'#bababa',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:5,
        borderWidth:3,
        borderColor:'#282828',
        margin:5
    },
    buttonText:{
        fontSize:20,
        fontWeight:'bold',
        color:'#282828'
    },
    img:{
        height:35,
        width:35
    }
};

export {Button, SmallButton};