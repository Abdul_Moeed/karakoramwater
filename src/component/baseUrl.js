// const BaseURL = 'http://192.168.10.12:4000/'         // Local IP
const BaseURL = 'https://kwaterserver.herokuapp.com/'  // heroku hosting


export const loginUrl = BaseURL+'login/userlogin'
export const signupUrl = BaseURL+'user/usersignup'
export const categoryUrl = BaseURL+'user/categories'