import { StyleSheet, Platform } from 'react-native';

const container = StyleSheet.create({
    container:{
            ...Platform.select({
                ios:{
                    marginTop:22
                }
            }),
            alignItems:'center',
            justifyContent:'center',
            backgroundColor:'#ced4ff',
            width:'100%',
            height:'100%'
        
    }
})

export default container