import React,{Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image, AsyncStorage, ScrollView} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'

import {clearAction} from '../redux/actions/clearAction'


import {Button} from './button'


class SidemenuItems extends Component{

    logoutAction(){
        AsyncStorage.removeItem('userId')
        this.props.clearAction()
        Actions.login()
    }


    render(){
        return(
            <View style={styles.container}>
            <ScrollView>
            <View style={styles.imgContainer}>
            <Image style={styles.img} source = {require('../assets/logo.png')}/>
            </View>
            <View style={styles.navigationContainer}>
                <TouchableOpacity style={styles.navigation}>
                    <Text style={styles.txt}>Account</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.navigation}>
                    <Text style={styles.txt}>Home</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.navigation}>
                    <Text style={styles.txt}>Booking</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.navigation}>
                    <Text style={styles.txt}>History</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.navigation}>
                    <Text style={styles.txt}>About us</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.navigation}>
                    <Text style={styles.txt}>Contact us</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.btnContainer}>

                <TouchableOpacity style={styles.btnNavigation} onPress={()=>{
                    Actions.login()
                    this.props.clearAction()
                    AsyncStorage.removeItem('userId')
                    AsyncStorage.removeItem('userName')
                    AsyncStorage.removeItem('Address')                    
                }}>
                
                    <Text style={styles.btnTxt}>Logout</Text>
                </TouchableOpacity>
            </View>
            </ScrollView>
            </View>
        )
    }
}

const styles ={
    container:{
        flex:3,
        width:'100%',
        height:'100%',
        backgroundColor:'#ffff',
        justifyContent:'flex-start',
        alignItems:'center'
    },
    imgContainer:{
        height:'30%',
        width:'100%',
        alignItems:'center',
        marginBottom:10
    },
    img:{
        width:150,
        height:150
    },
    navigationContainer:{
        height:'50%',
        width:'100%',
        justifyContent:'center',
        alignItems:'center'
    },
    navigation:{
        height:20,
        width:'70%',
        borderBottomWidth:2,
        borderBottomColor:'#bababa',
        justifyContent:'center',
        marginBottom:30
    },
    txt:{
        fontSize:20,
        fontWeight:'600'
    },
    btnContainer:{
        height:'20%',
        width:'100%',
        // justifyContent:'center',
        alignItems:'center'
    },
    btnNavigation:{
        height:50,
        width:'70%',
        borderColor:'#bababa',
        borderWidth:2,
        borderRadius:5,
        justifyContent:'center',
        alignItems:'center'
    },
    btnTxt:{
        fontSize:20,
        fontWeight:'700'
    }
}
function mapDispatchToProps(dispatch){
    return bindActionCreators({
        clearAction
    },dispatch)
}

export default connect(null, mapDispatchToProps)(SidemenuItems);
