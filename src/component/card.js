import React from 'react';
import {TouchableOpacity, ScrollView, Image, Text, Dimensions} from 'react-native'

const Card = (props)=>{
    return(
        
        <TouchableOpacity style={styles.box} onPress={props.navigation}>
        <Image style={styles.img} source={props.source}/>
        <Text style={styles.text}>{props.text}</Text>
        <Text style={styles.text}>{props.text1}</Text>
        </TouchableOpacity>
        
    )
}

const styles = {
    box:{
        margin:2,
        width:Dimensions.get('window').width / 2 -6,
        height:250,
        justifyContent:'center',
        alignItems: 'center',
        borderWidth:1,
        borderColor:'steelblue',
    },
    img:{
        height:150,
        width:150
    },
    text:{
        width:'90%',
        borderWidth:1,
        borderColor:'black',
        textAlign:'center',
        fontSize:16,
        marginBottom:5
    }
}

export {Card}