import React from 'react';
import {View, Text, Platform, Image, TouchableOpacity} from 'react-native';



const Headbar = (props) => {
    return(
        <View style={styles.headView}>
        <TouchableOpacity style={styles.press} onPress={props.navigation}>
        <Image style={styles.img} source = {props.source}/>
        </TouchableOpacity>
            <Text style={styles.headText}>{props.text}</Text>
        </View>
    )
}

const styles = {
    headView:{
        flexDirection:'row',
        ...Platform.select({
            ios:{
                height:64
            },
            android:{
                height:56
            }
        }),
        width:'100%',
        backgroundColor:'#bababa',
        alignItems:'center',
        
    },
    headText:{
        fontSize:25,
        fontWeight:'bold',
        color:'#282828',
        width:'80%',
        textAlign:'center'
    },
    img:{
        ...Platform.select({
            ios:{
                height:60,
                width:'30%'
            },
            android:{
                height:50,
                width:'30%'
            }
        }),
        justifyContent:'center'
    },
    press:{
        ...Platform.select({
            ios:{
                height:60,
                width:60
            },
            android:{
                height:50,
                width:50
            },
            
        }),
        paddingLeft:20
    }
};

export {Headbar};