import React, {Component} from 'react';
import {View, StyleSheet, Image, Platform, TextInput, Text, TouchableOpacity, ToastAndroid, AsyncStorage} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import {Spinner, Headbar, Button} from './component';
import container from './component/stylesheet';

import {loginAction} from './redux/actions/loginAction'
import {clearAction} from './redux/actions/clearAction'

class login extends Component {

    constructor(){
        super();
        this.state = {
            phoneNumber:'',
            password:'',
            loading:false
        }
        this.handleLogin = this.handleLogin.bind(this)
    }

    handleLogin() {
        const login = {
            PhoneNumber: this.state.phoneNumber,
            password: this.state.password
        };
        console.log(login)
        this.props.loginAction(login);
    }


    renderButton(){
        if(this.state.loading === true){
            return <Spinner size = 'small'/>;
        }
         return (
            <Button text={'Login'} navigation={()=>{
                this.handleLogin(),
                this.setState({loading:true})
            }}/>
         )
    }

    render(){
        this.props.login.map((x)=>{
            if(x.Status === 'False'){
               ToastAndroid.show('Invalid Credentials', ToastAndroid.LONG)
               this.setState({loading:false})
               this.props.clearAction()
               }
               else
               {
                ToastAndroid.show('Login Success!', ToastAndroid.SHORT);
                AsyncStorage.setItem('userId', x.data.userid);
                AsyncStorage.setItem('userName', x.data.username);
                AsyncStorage.setItem('Address', x.data.Address)
                Actions.home()
               }
        })

        return(
            <View style={container.container}>
                <Headbar text={'Login'}/>
                <View style={styles.form}>
                    <View style={styles.inputForm}>
                    <Image style={styles.img} source={require('./assets/logo.png')}/>
                    <TextInput
                    style={styles.input}
                    placeholder= 'Phone Number'
                    placeholderTextColor= 'black'
                    keyboardType='phone-pad'
                    autoCapitalize= 'none'
                    returnKeyLabel= 'Next'
                    onChangeText={(text)=>this.setState({phoneNumber:text})}
                    onSubmitEditing={(event)=>{this.refs.Second.focus()}}

                    />
                    <TextInput
                    style={styles.input}
                    ref= 'Second'
                    placeholder={'Password'}
                    secureTextEntry={true}
                    returnKeyLabel= 'Done'
                    autoCapitalize='none'
                    placeholderTextColor={'black'}
                    onChangeText={(text)=>this.setState({password:text})}
                    />
                    </View>
                    <View style={styles.inputForm}>
                    {this.renderButton()}
                    <Text onPress={Actions.signup} style={styles.sign}>Not Register? Tap Here</Text>
                    </View>
                </View>

                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    form:{
        flex:2,
        height:'100%',
        width:'100%',
        justifyContent:'center',
        alignItems:'center'
    },
    inputForm:{
        width:'100%',
        height:'50%',
        justifyContent:'center',
        alignItems:'center'
    },
    input:{
        width:'80%'
    },
    sign:{
        fontSize:16,
        marginTop:30,
        textDecorationLine:'underline'
    },
    img:{
        width:100,
        height:100
    }

})

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        loginAction,
        clearAction
    },dispatch)
}

function mapStateToProps(state){
    return{
        login:state.login.login
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(login)