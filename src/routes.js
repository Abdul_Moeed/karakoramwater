import React from 'react';
import {Router, Scene} from 'react-native-router-flux';
import {Provider} from 'react-redux';
import {logger} from 'redux-logger';
import thunk from 'redux-thunk';
import {createStore, applyMiddleware} from 'redux';

import Reducers from './redux/reducers/index'

import splash from './splash';
import login from './login';
import signup from './signup';
import home from './home';
import quantity from './quantity';
import form from './form'



const middleware = applyMiddleware(logger,thunk);
const store = createStore(Reducers, middleware);

const routes = () => (
    <Provider store = {store}>
<Router>
    <Scene key = 'root'>
        <Scene key="splash" component = {splash} hideNavBar={true} initial={true}/>
        <Scene key="login" component = {login} hideNavBar={true}/>
        <Scene key="signup" component = {signup} hideNavBar={true}/>
        <Scene key="home" component = {home} hideNavBar={true}/>
        <Scene key="quantity" component={quantity} hideNavBar={true}/>
        <Scene key="form" component={form} hideNavBar={true}/>
    </Scene>
</Router>
</Provider>
)

export default routes;