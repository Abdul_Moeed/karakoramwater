import React, {Component} from 'react';
import {View, StyleSheet, TextInput, Text, KeyboardAvoidingView, ToastAndroid} from 'react-native'
import {Actions} from 'react-native-router-flux'
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import {Spinner, Headbar, Button} from './component'
import container from './component/stylesheet'

import {signupAction} from './redux/actions/signupAction'
import {clearAction} from './redux/actions/clearAction'

class signup extends Component {

    constructor(){
        super();
        this.state={
            userName:'',
            phoneNumber:'',
            password:'',
            repeatpassword:'',
            address:'',
            loading:false
        }
        this.handlesignup = this.handlesignup.bind(this)
    }

    validateName = (userName) =>{
        var re =  /^[a-zA-Z ]{2,30}$/
        return re.test(userName)
    }
    validatePassword = (password) =>{
        var re = /^.{6,}/
        return re.test(password)
    };
    validatePhone = (phoneNumber) => {
        var re = /^.{9,}/
        return re.test(phoneNumber)
    };

    handlesignup(){
        const signup = {
            UserName:this.state.userName,
            password:this.state.password,
            Address:this.state.address,
            PhoneNumber:this.state.phoneNumber
        }
        console.log(signup)
        this.props.signupAction(signup)
    }

    renderButton(){
        if (this.state.loading){
            return <Spinner/>;
        }
        return(
            <Button text={'Signup'} navigation={()=>{
                if(this.state.password != this.state.repeatpassword){
                    alert('Password is not matched')
                }
                if(!this.validateName(this.state.userName)){
                    alert('Invalid user name')
                }
                if(!this.validatePassword(this.state.password)){
                    alert('Password Must be more than 6 characters')
                }
                if(!this.validatePhone(this.state.phoneNumber)){
                    alert('Invalid phone number')
                }
                else {
                    this.handlesignup()
                    this.setState({loading:true})
                }
            }}/>
        )
    }

    render(){
        this.props.signup.map((x)=>{
            if(x.Status === 'True'){
                ToastAndroid.show('User created successfully', ToastAndroid.SHORT)
                Actions.login()
            }
            else{
                alert('Phone number already exist')
                this.setState({loading:false})
                this.props.clearAction()
            }
        })
        return(
            <View style={container.container}>
            <Headbar text={'SignUp'} />
            <View style={styles.form}>
                    <View style={styles.inputForm}>
                    <TextInput
                    style={styles.input}
                    placeholder= 'User Name'
                    placeholderTextColor= 'black'
                    autoCapitalize= 'words'
                    returnKeyLabel= 'Next'
                    onChangeText={(text)=> this.setState({userName:text})}
                    onSubmitEditing={(event)=>{this.refs.Second.focus()}}

                    />
                    <TextInput
                    style={styles.input}
                    ref= 'Second'
                    placeholder={'Phone Number'}
                    keyboardType='phone-pad'
                    returnKeyLabel= 'Next'
                    autoCapitalize='none'
                    placeholderTextColor={'black'}
                    onChangeText={(text)=> this.setState({phoneNumber:text})}
                    onSubmitEditing={(event)=>{this.refs.Third.focus()}}
                    />

                    <TextInput
                    style={styles.input}
                    ref= 'Third'
                    placeholder={'Password'}
                    secureTextEntry={true}
                    returnKeyLabel= 'Next'
                    autoCapitalize='none'
                    placeholderTextColor={'black'}
                    onChangeText={(text)=> this.setState({password:text})}
                    onSubmitEditing={(event)=>{this.refs.Forth.focus()}}
                    />

                    <TextInput
                    style={styles.input}
                    ref= 'Forth'
                    placeholder={'Repeat Password'}
                    secureTextEntry={true}
                    returnKeyLabel= 'Next'
                    autoCapitalize='none'
                    placeholderTextColor={'black'}
                    onChangeText={(text)=> this.setState({repeatpassword:text})}
                    onSubmitEditing={(event)=>{this.refs.Fifth.focus()}}
                    />

                    <TextInput
                    style={styles.input}
                    ref= 'Fifth'
                    placeholder={'Address'}
                    returnKeyLabel= 'Done'
                    autoCapitalize='none'
                    placeholderTextColor={'black'}
                    onChangeText={(text)=> this.setState({address:text})}
                    />

                    
                    </View>
                    <View style={styles.buttonForm}>
                    {this.renderButton()}
                    <Text onPress={Actions.login} style={styles.sign}>Already SignUp? Tap Here</Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    form:{
        flex:2,
        height:'100%',
        width:'100%',
        justifyContent:'center',
        alignItems:'center'
    },
    inputForm:{
        width:'100%',
        height:'70%',
        justifyContent:'center',
        alignItems:'center'
    },
    input:{
        width:'80%'
    },
    sign:{
        fontSize:16,
        marginTop:30,
        textDecorationLine:'underline'
    },
    img:{
        width:100,
        height:100
    },
    buttonForm:{
        width:'100%',
        height:'30%',
        justifyContent:'center',
        alignItems:'center'
    }

})

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        signupAction,
        clearAction
    },dispatch)
}

function mapStateToProps(state){
    return{
        signup:state.signup.signup
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(signup);