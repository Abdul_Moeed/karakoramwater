import React, {Component} from 'react';
import {View, Modal, ScrollView, StyleSheet, TextInput, Text, TouchableHighlight, Dimensions, Image, AsyncStorage} from 'react-native';
import {Actions} from 'react-native-router-flux';
import DrawerLayout from 'react-native-drawer-layout'

import {Spinner,Button ,SmallButton, Headbar} from './component';
import container from './component/stylesheet';
import SidemenuItems from './component/sidemenuItems'

class form extends Component{
    constructor(){
        super();
        this.state = {
            modalVisible: false,
            userId:'',
            Name:'',
            Address:'',
            PhoneNumber:'',
            comment:'',
            type:'',
            quantity:null,
            price:null,
            totalPrice:null
        }
    }

    componentDidMount(){
        AsyncStorage.getItem('userId').then((x)=>{
            this.setState({userId:x})
        })
        AsyncStorage.getItem('userName').then((x)=>{
            this.setState({Name:x})
        })
        AsyncStorage.getItem('Address').then((x)=>{
            this.setState({Address:x})
        })
        AsyncStorage.getItem('type').then((x)=>{
            this.setState({type:x})
        })
        AsyncStorage.getItem('quantity').then((x)=>{
            let qunt = Number(x)
            this.setState({quantity:qunt})
        })
        AsyncStorage.getItem('price').then((x)=>{
            let price = Number(x)
            this.setState({price:price})
        })    
        setTimeout(()=>{
            let q = this.state.quantity
            let p = this.state.price
            let tPrice = q * p
            this.setState({totalPrice:tPrice})
        },1000)
    }
     toggleModal(visible) {
        this.setState({ modalVisible: visible });
     }

    render(){
        const sidemenu = <SidemenuItems/>
        return(
            <View style={container.container}>
            <DrawerLayout
                drawerWidth={220}
                ref={(drawer)=>{return this.drawer = drawer}}
                keyboardDismissMode='on-drag'
                renderNavigationView={()=> sidemenu}>
            <Modal animationType = {"fade"} transparent = {true}
        visible = {this.state.modalVisible}
        onRequestClose = {()=> { console.log("Modal has been closed.") } }>
        <Headbar text={'Confirmation'}/>
        <View style={modal.container}>
        <TouchableHighlight style={modal.top} onPress={() => {this.toggleModal(!this.state.modalVisible)}}>
        <Image source={require('./assets/cross.png')} style={modal.img}/>
        </TouchableHighlight>
        <View style={modal.info}>
            <Text style={modal.txt}>You Have Ordered: {this.state.quantity} Bottles</Text>
            <Text style={modal.txt}>Total price: {this.state.totalPrice} Pkr</Text>
            <Text style={modal.txt}>Address: {this.state.Address}</Text>
        </View>
        <View style={modal.buttons}>
        <SmallButton text={'Confirm Order'} navigation={() => {this.toggleModal(!this.state.modalVisible) || Actions.home()}}/>
        <SmallButton text={'Cancel Order'} navigation={() => {this.toggleModal(!this.state.modalVisible)|| Actions.quantity()}}/>
        </View>

        </View>
        </Modal>
        <Headbar text={'Please fill the form'} source={require('./assets/menu.png')} navigation={() => this.drawer.openDrawer()}/>
            <View style={styles.form}>
            <View style={styles.inputForm}>
                <TextInput
                    style={styles.input}
                    defaultValue={this.state.Name}
                    placeholder='Full Name'
                    placeholderTextColor='gray'
                    autoCapitalize='words'
                    returnKeyLabel='Next'
                    onChangeText={(text)=>this.setState({Name:text})}
                    onSubmitEditing={(event)=>{this.refs.second.focus()}}
                    />
                <TextInput
                    style={styles.input}
                    defaultValue={this.state.Address}
                    ref='second'
                    placeholder='Address'
                    placeholderTextColor='gray'
                    returnKeyLabel='Next'
                    onChangeText={(text)=>this.setState({Address:text})}
                    onSubmitEditing={(event)=>{this.refs.second.focus()}}
                    />
                <TextInput
                    style={styles.input}
                    ref='third'
                    placeholder='Phone Number'
                    keyboardType='phone-pad'
                    placeholderTextColor='gray'
                    autoCapitalize='none'
                    returnKeyLabel='Next'
                    onChangeText={(text)=>this.setState({PhoneNumber:text})}
                    onSubmitEditing={(event)=>{this.refs.forth.focus()}}
                    />
                <TextInput
                    maxLength={60}
                    numberOfLines={2}
                    style={styles.inputComment}
                    ref='forth'
                    placeholder='Comment (Optional)'
                    placeholderTextColor='gray'
                    autoCapitalize='none'
                    returnKeyLabel='Done'
                    onChange={(text)=>this.setState({comment:text})}
                    />
            </View>
            <View style={styles.buttonForm}>
            <Button navigation = {() => {this.toggleModal(true)}} text={'Order it!'}/>
            </View>
            </View>
            </DrawerLayout>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    form:{
        flex:2,
        justifyContent:'center',
        alignItems:'center',
        width:Dimensions.get('window').width
    },
    inputForm:{
        height:'80%',
        width:'100%',
        justifyContent:'center',
        alignItems:'center'
    },
    buttonForm:{
        height:'20%',
        width:'100%',
        justifyContent:'center',
        alignItems:'center'
    },
    input:{
        width:'80%'
    },
    inputComment:{
        width:'80%',
        backgroundColor:'#eff1ff',
        borderRadius:5,
        marginTop:10
    },
})

const modal = StyleSheet.create({
    container:{
        flex: 3,
        alignItems: 'center',
        borderRadius:10,
        backgroundColor: '#dbdfff',
        margin:'6%'
    },
    top:{
        width:'100%',
        height:'10%',
        justifyContent:'center',
        alignItems:'flex-end'
    },
    img:{
        height:40,
        width:40
    },
    info:{
        height:'80%',
        justifyContent:'center',
        borderColor:'#ced4ff',
        borderRadius:10,
        borderWidth:2
    },
    txt:{
        fontSize:16,
        margin:10,
        color:'black'
    },
    buttons:{
        height:'15%',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center'
    }
})

export default form;