import React,{Component} from 'react';
import {View, Text, Image, StyleSheet, Dimensions, TouchableOpacity, AsyncStorage} from 'react-native';
import DrawerLayout from 'react-native-drawer-layout'
import {Headbar, Spinner, Button, Card} from './component';
import container from './component/stylesheet'
import { Actions } from 'react-native-router-flux';
import SidemenuItems from './component/sidemenuItems';

class quantity extends Component{

    constructor(){
        super();
        this.state = {
            quantity:3
        }
    }
    componentDidMount(){
        AsyncStorage.setItem('quantity', this.state.quantity)
    }

    decreament(){
        let x = this.state.quantity
        if(x <= 1){
            return alert('Please Select Atleast One Gallon')
        }
        else {
            x = x-1 ;
            this.setState({quantity:x})
            AsyncStorage.setItem('quantity', String(x))
        }
    }

    increament(){
        let x = this.state.quantity
        if(x >= 20){
            return alert('Please Select Atmost 20 Gallons')
        }
        else {
            x = x+1 ;
            this.setState({quantity:x})
            AsyncStorage.setItem('quantity', String(x))
        }
    }

    quantity(){
        Actions.form()
    }

    render(){
        const sidemenu = <SidemenuItems/>
        return(
            <View style={container.container}>
            <DrawerLayout
                drawerWidth={220}
                ref={(drawer)=>{return this.drawer = drawer}}
                keyboardDismissMode='on-drag'
                renderNavigationView={()=> sidemenu}>
                <Headbar text={'Please Select Quantity'} source={require('./assets/menu.png')} navigation={() => this.drawer.openDrawer()}/>
            <View style={styles.container}>
            <Card source={require('./assets/logo.png')} text={'Mineral 19 ltr'}/>
            <View style={styles.quantity}>
            <TouchableOpacity onPress={()=>this.decreament()}>
                <Image style={styles.img} source={require('./assets/minus.png')}/>
                </TouchableOpacity>
                <Text style={styles.txt}>{this.state.quantity}</Text>
                <TouchableOpacity onPress={()=>this.increament()}>
                <Image style={styles.img} source={require('./assets/plus.png')}/>
                </TouchableOpacity>
            </View>
            <Button text={'Book it!'} navigation={()=> this.quantity()}/>
            </View>
            </DrawerLayout>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:2,
        // justifyContent:'center',
        alignItems:'center',
        marginTop:20,
        width:Dimensions.get('window').width
    },
    quantity:{
        marginTop:50,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        marginBottom:100
    },
    img:{
        height:50,
        width:50
    },
    txt:{
        width:Dimensions.get('window').width / 2 -6,
        marginLeft:10,
        marginRight:10,
        borderBottomWidth:2,
        textAlign:'center',
        fontSize:40,
        fontWeight:'bold'
    }
})

export default quantity;